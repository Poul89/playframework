package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import play.core.j.HttpExecutionContext;
import play.libs.Json;
import play.mvc.*;
import models.Book;
import services.BookService;
import services.DefaultBookService;
import javax.inject.Inject;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

public class BookController extends Controller {

    private final BookService bookService;

    @Inject
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    public CompletionStage<Result> getAll(String q) {
        return bookService.get().thenApplyAsync(bookStream ->
                ok(Json.toJson(bookStream.collect(Collectors.toList())))
        );
    }

    public CompletionStage<Result> get(Long id) {
        return bookService.get(id).thenApplyAsync(book -> ok(Json.toJson(book)));
    }

    /*public CompletionStage<Result> search(String title) {
            return ok("Results for " + title);
    }
    */

    public CompletionStage<Result> add(Http.Request request) {
        JsonNode json = request.body().asJson();
        Book bookAdd = Json.fromJson(json, Book.class);
        return bookService.add(bookAdd).thenApplyAsync(book -> ok(Json.toJson(book)));
    }

    //wird hier direkt das Json geliefert oder muss man die id noch mitliefern?
    public CompletionStage<Result> update(Long id, Http.Request request) {
        JsonNode json = request.body().asJson();
        Book bookUpdate = Json.fromJson(json, Book.class);
        bookUpdate.setId(id);
        return bookService.update(bookUpdate).thenApplyAsync(book -> ok(Json.toJson(book)));
    }

    public CompletionStage<Result> delete(Long id) {
        return bookService.delete(id).thenApplyAsync(removed -> removed ? ok() : internalServerError());
    }
}
