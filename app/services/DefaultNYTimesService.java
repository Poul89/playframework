package services;

import com.fasterxml.jackson.databind.JsonNode;
import models.nytimes.NYTimesBestseller;
import models.nytimes.NYTimesBestsellerResults;
import models.nytimes.NYTimesBook;
import models.nytimes.NYTimesBuyLinks;
import models.nytimes.NYTimesReviewList;
import play.libs.Json;
import play.libs.ws.*;
import models.Book;
import com.typesafe.config.Config;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionStage;
import javax.inject.Inject;

public class DefaultNYTimesService implements NYTimesService, WSBodyReadables{

    private WSClient ws;
    private String apikey;

    @Inject
    public DefaultNYTimesService(WSClient ws, Config config){
        this.ws = ws;
        this.apikey = config.getString("nytimes.api.key");
    }

    @Override
    public CompletionStage<List<Book>> bestseller() {
        final String url = "https://api.nytimes.com/svc/books/v3/lists/overview.json";
        final WSRequest request = ws.url(url).addQueryParameter("api-key", apikey);
        final CompletionStage<JsonNode> promise = request.get().thenApply(result -> result.getBody(json()));
        final CompletionStage<NYTimesBestseller> bsPromise = promise.thenApplyAsync(json -> Json.fromJson(json, NYTimesBestseller.class));
        final CompletionStage<List<Book>> bookPromise = bsPromise.thenApplyAsync(bestseller -> map(bestseller));
        return bookPromise;
    }

    private List<Book> map(NYTimesBestseller bestseller) {
        final List<Book> books = new ArrayList<>();
        for(NYTimesReviewList list: bestseller.getResults().getLists()) {
            if(list.getList_id() == 704) {
                list.getBooks().forEach(nyTimesBook -> books.add(map(nyTimesBook)));
            }
        }
        return books;
    }

    private Book map(NYTimesBook nyTimesBook) {
        final Book book = new Book();
        book.setTitle(nyTimesBook.getTitle());
        book.setIsbn13(nyTimesBook.getPrimary_isbn13());
        book.setIsbn10(nyTimesBook.getPrimary_isbn10());
        book.setDescription(nyTimesBook.getDescription());
        book.setPublisher(nyTimesBook.getPublisher());
        return book;
        }
    }
