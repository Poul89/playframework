package services;

import models.Book;
import models.BookRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;
import javax.inject.Inject;

public class DefaultBookService implements BookService{

    private BookRepository bookRepository;

    @Inject
    public DefaultBookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public CompletionStage<Stream<Book>> get() {
        return bookRepository.list();
    }

    public CompletionStage<Book> get(final Long id) {
        return bookRepository.find(id);
    }

    public CompletionStage<Boolean> delete(final Long id) {
       return bookRepository.remove(id);
    }

    public CompletionStage<Book> update(final Book updatedBook) {
        return bookRepository.update(updatedBook);
    }

    public CompletionStage<Book> add(final Book book) {
        return bookRepository.add(book);
    }
}
